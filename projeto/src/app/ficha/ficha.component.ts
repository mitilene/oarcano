import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.component.html',
  styleUrls: ['./ficha.component.scss']
})
export class FichaComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  terceiroFormGroup: FormGroup;
  generoControl: string;
  olhosControl: string;
  
  // formatLabel(value: number) {
  //   if (value >= 4) {
  //     return value + ': Instável (Lesões instáveis pioraram com o tempo)';
  //     // return Math.round(value) + 'Instável (Lesões instáveis pioraram com o tempo)';
  //   }

  //   return value;
  // }
  
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

}
